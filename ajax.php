<?php
include_once("db_access.php");


http_response_code(200);


$id = isset($_POST["id"]) ? $_POST["id"] : "";
$date = isset($_POST["date"]) ? $_POST["date"] : "";

$rows = execute_sql(
	"SELECT count(*) AS `cnt` FROM `dates` WHERE `id` = ?;",
	array($id)
);

if($rows[0]["cnt"] == 0 ){

	$ret = execute_sql(
		"INSERT INTO `dates` (`id`, `date`, `deleted`) VALUES ( ?, ?, ?);",
		array($id, $date, false)
	);

}else {
	
	$ret = execute_sql(
		"UPDATE `dates` SET `date` = ? WHERE id = ?",
		array($date, $id)
	);
}


$result = array(
	"status" => (int) true,
	
	"id" => $id,
	"date" => $date,
);

//var _dump($result);

echo json_encode($result);


/*


　　　　　 　　　　　			彡ﾉﾉﾊミ　　　　こらあああああああ
　　　　　　　　　　　　 　　　(´･ω･｀)　
　　　　　　 		＼＿＿＿＿＿／　　｜　　　　　何見とんじゃあああああ
　　　　　　 　 |　　　　　　 　　　　｜
　　　　　　　　＼　　　　　　　　　ノ 　
　　　　　（（（　　(/￣￣￣￣(/ヽ)


　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　 　彡ﾉﾉﾊミ
　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　 　(´･ω･｀)　　　　　見ておもろいんかごらあああああああああああ
　　　　　　　　　　　　　　　　　　　　　　　 ＼＿＿＿＿＿／　　｜
　　　　　　　　　　　　　　　　　　　　　　　 　 |　　　　　　 　　　　｜
　　　　　　　　　　　　　　　　　　　　　　　　　＼　　　　　　　　　ノ 　
　　　　　　（（（（（（（（（（（（（（（（（（（（（（（（　　(/￣￣￣￣(/ヽ)


　　　　　　　|
　 　 　 　 ／＼
.　　 　 　 l　 　 ＼　　　
.　　　　 (/＼　　　＼
　　　　　彡　＼　　　＼彡⌒ ミ　　　　　　　　　　　くそがあああああああ
　　　　　彡　＼　　　　(´･ω･｀)
￣￣￣￣￣￣|＼　　_,／|￣￣￣￣￣￣
　 　 　 　 　 　 | (/ ヽ)　　|
　 　 　 　 　 　 | ミ　 彡　.|
　　　　　　　　 |　　　　　　|
　　　　　　　　 |　彡ﾉﾉﾊミ|

*/